# translation of popa3d debconf to Portuguese
# Copyright (C) 2007 Américo Monteiro
# This file is distributed under the same license as the popa3d package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: popa3d 1.0.2-3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-01-13 20:07+0100\n"
"PO-Revision-Date: 2007-08-02 01:46+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Run popa3d in standalone mode?"
msgstr "Correr o popa3d em modo 'standalone'?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"In standalone mode, popa3d will become a daemon, accepting connections on "
"the pop3 port (110/tcp) and forking child processes to handle them. This has "
"lower overhead than starting popa3d from an inetd equivalent and is thus "
"useful on busy servers to reduce load. See popa3d(8) for more details."
msgstr ""
"Em modo 'standalone', o popa3d irá tornar-se num processo independente "
"(daemon), aceitando ligações no porto pop3 (110/tcp) e sustendo processos "
"filhos para as controlar. Isto tem uma sobrecarga menor do que arrancar o "
"popa3d de um equivalente inetd e mais útil em servidores muito ocupados "
"para reduzir a carga. Veja popa3d(8) para mais detalhes."

