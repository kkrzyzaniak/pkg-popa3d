#! /bin/sh
### BEGIN INIT INFO
# Provides:          popa3d
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start the popa3d server.
### END INIT INFO


test -f /usr/sbin/popa3d || exit 0

DAEMON=/usr/sbin/popa3d
NAME=popa3d
DEFAULT=/etc/default/popa3d
ARGS="-D"
PIDFILE=/var/run/popa3d.pid

#. /etc/default/popa3d
. /lib/lsb/init-functions

# Check for default file, if it is not there or it says no start
# then bomb out

if [ -e /etc/default/popa3d ]; then
    . /etc/default/popa3d
    case $RUN_STANDALONE in
	yes|1|true)
	    ;;
	*)
	    exit 0
	    ;;
    esac
fi


case "$1" in
start|force-reload)

  echo -n "Starting pop daemon: "
  start-stop-daemon --start --name popa3d --exec $DAEMON -- $ARGS
  echo `pidof popa3d` > $PIDFILE
  echo ${NAME}.
  ;;
stop)
  echo -n "Stopping pop daemon: "
  start-stop-daemon --stop --name popa3d --pidfile $PIDFILE
  echo ${NAME}.
  ;;

reload|restart)
  echo -n "Stopping pop daemon: "
  start-stop-daemon --stop --name popa3d --pidfile $PIDFILE
  echo ${NAME}.
  echo -n "Starting pop daemon: "
  start-stop-daemon --start --name popa3d --exec $DAEMON -- $ARGS
  echo ${NAME}.
  ;;

*)
  echo "Usage: /etc/init.d/popa3d {start|stop|restart|force-reload}"
  exit 1
esac

exit 0
